/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * This is a bare minimum user application template.
 *
 * For documentation of the board, go \ref group_common_boards "here" for a link
 * to the board-specific documentation.
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to system_init()
 * -# Basic usage of on-board LED and button
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <tc.h>
#include <dac.h>
#include <dma.h>
#include <math.h>
#include <gclk.h>

#define PWM_MODULE			EXT1_PWM_MODULE
#define PWM_OUT_PIN			EXT1_PWM_0_PIN
#define PWM_OUT_MUX			EXT1_PWM_0_MUX
#define M2M_DMAC_TRIGGER_ID TC6_DMAC_ID_MC_0

#define arr_size(f_dac, f_sig) f_dac / f_sig
#define time_step(dac_freq) 1.0 / dac_freq

static const int frequency_signal = 250;
static const int dac_frequency = 256000;
static float *nr_of_samples;
static int m = 0;

struct tc_module tc_instance;
struct dac_module	dac_instance;
struct dma_resource dac_dma_resource;
COMPILER_ALIGNED(16)
DmacDescriptor example_descriptor_dac;

void configure_tc(void)
{
	struct tc_config config_tc;
	tc_get_config_defaults(&config_tc);
	
	config_tc.counter_size = TC_COUNTER_SIZE_16BIT;
	config_tc.wave_generation = TC_WAVE_GENERATION_NORMAL_PWM;
	config_tc.clock_source = GCLK_GENERATOR_1;
	config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV1024;
	config_tc.counter_16_bit.compare_capture_channel[0] = (0xFFFF / 419.424);
	config_tc.pwm_channel[0].enabled = true;
	config_tc.pwm_channel[0].pin_out = PWM_OUT_PIN;
	config_tc.pwm_channel[0].pin_mux = PWM_OUT_MUX;
	config_tc.reload_action = TC_RELOAD_ACTION_PRESC;
	
	tc_init(&tc_instance, PWM_MODULE, &config_tc);
	tc_enable(&tc_instance);
}

void generate_sine_values(void)
{
	nr_of_samples = malloc(sizeof(*nr_of_samples) * arr_size(dac_frequency, frequency_signal));
	for (int i = 0; i <arr_size(dac_frequency, frequency_signal); i++)
	{
		nr_of_samples[i] = 310 * sin(2 * M_PI * frequency_signal*(i * time_step(dac_frequency))) + 310;
	}
}

void callback_transfer_done_DAC(struct dma_resource* const resource)
{
}

void configure_dac(void)
{
	struct dac_config config_dac;
	dac_get_config_defaults(&config_dac);
	
	config_dac.output = DAC_OUTPUT_EXTERNAL;
	config_dac.reference = DAC_REFERENCE_AREF;
	
	dac_init(&dac_instance, DAC, &config_dac);
	
	dac_enable(&dac_instance);
}

void configure_dac_channel (void)
{
	struct dac_chan_config config_dac_chan;
	dac_chan_get_config_defaults(&config_dac_chan);
	
	dac_chan_set_config(&dac_instance, DAC_CHANNEL_0, &config_dac_chan);
	dac_chan_set_config(&dac_instance, DAC_REFERENCE_AREF, &config_dac_chan);
	
	dac_chan_enable(&dac_instance, DAC_CHANNEL_0);
}

void configure_dma_dac_resource(struct dma_resource *resource)
{
	struct dma_resource_config config;
	dma_get_config_defaults(&config);
	
	config.peripheral_trigger = M2M_DMAC_TRIGGER_ID;
	config.trigger_action = DMA_TRIGGER_ACTION_BEAT;
	
	dma_allocate(resource, &config);
}

void setup_transfer_dac_descriptor(DmacDescriptor *descriptor)
{
	struct dma_descriptor_config descriptor_config;
	dma_descriptor_get_config_defaults(&descriptor_config);
	
	descriptor_config.beat_size = DMA_BEAT_SIZE_HWORD;
	descriptor_config.block_transfer_count = arr_size(dac_frequency, frequency_signal);
	descriptor_config.dst_increment_enable = false;
	//descriptor_config.block_action = DMA_BLOCK_ACTION_INT;
	descriptor_config.source_address = (uint32_t) nr_of_samples + arr_size(dac_frequency, frequency_signal);
	descriptor_config.destination_address = (uint32_t)(&dac_instance.hw->DATA.reg);
	descriptor_config.next_descriptor_address = 0;
	
	dma_descriptor_create(descriptor, &descriptor_config);
}

int main (void)
{
	system_init();
	configure_tc();
	configure_dac();
	configure_dac_channel();
	
	generate_sine_values();
	
	configure_dma_dac_resource(&dac_dma_resource);
	setup_transfer_dac_descriptor(&example_descriptor_dac);
	
	dma_register_callback(&dac_dma_resource, callback_transfer_done_DAC, DMA_CALLBACK_TRANSFER_DONE);
	dma_enable_callback(&dac_dma_resource, DMA_CALLBACK_TRANSFER_DONE);
	
	while(true)
	{
		if (m < arr_size(dac_frequency, frequency_signal))
		{
			dma_start_transfer_job(&dac_dma_resource);
			while (dma_is_busy(&dac_dma_resource));
			m++;
			} else {
			m = 0;
		}
	}
	
	return 0;
}